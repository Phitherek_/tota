FactoryBot.define do
  factory :authentication_token do
    instance
    user
  end
end
