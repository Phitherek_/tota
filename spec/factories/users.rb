FactoryBot.define do
  factory :user do
    sequence(:login) do |n|
      "testuser#{n}"
    end
    callsign do
      cs = "#{%w(SO SP SQ HF 3Z).sample}#{(0..9).to_a.sample}#{('A'..'Z').to_a.sample((1..4).to_a.sample).join('')}"
      while User.where(callsign: cs).any?
        cs = "#{%w(SO SP SQ HF 3Z).sample}#{(0..9).to_a.sample}#{('A'..'Z').to_a.sample((1..4).to_a.sample).join('')}"
      end
      cs
    end
    password { 'testtest' }
    password_confirmation { 'testtest' }
    admin { false }
    trait :admin do
      admin { true }
    end
  end
end
