FactoryBot.define do
  factory :log_entry do
    user
    instance
    band { '2m' }
    mode { 'FM' }
    qso_at { Time.now.utc }
    trait :activation do
      callsign_from do
        cs = "#{%w(SO SP SQ HF 3Z).sample}#{(0..9).to_a.sample}#{('A'..'Z').to_a.sample((1..4).to_a.sample).join('')}"
        while LogEntry.where(callsign_from: cs).any?
          cs = "#{%w(SO SP SQ HF 3Z).sample}#{(0..9).to_a.sample}#{('A'..'Z').to_a.sample((1..4).to_a.sample).join('')}"
        end
        cs
      end
      callsign_to { user.callsign }
      toilet_id_to { ToiletDefinition.order(Arel.sql('RANDOM()')).first.identifier }
    end
    trait :chase do
      callsign_from do
        cs = "#{%w(SO SP SQ HF 3Z).sample}#{(0..9).to_a.sample}#{('A'..'Z').to_a.sample((1..4).to_a.sample).join('')}"
        while LogEntry.where(callsign_from: cs).any?
          cs = "#{%w(SO SP SQ HF 3Z).sample}#{(0..9).to_a.sample}#{('A'..'Z').to_a.sample((1..4).to_a.sample).join('')}"
        end
        cs
      end
      callsign_to { user.callsign }
      toilet_id_from { ToiletDefinition.order(Arel.sql('RANDOM()')).first.identifier }
    end
    trait :activation_ttt do
      toilet_id_from { ToiletDefinition.order(Arel.sql('RANDOM()')).first.identifier }
    end
    trait :chase_ttt do
      toilet_id_to { ToiletDefinition.order(Arel.sql('RANDOM()')).first.identifier }
    end
  end
end
