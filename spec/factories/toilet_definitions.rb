FactoryBot.define do
  factory :toilet_definition do
    identifier do
      id = "#{('A'..'Z').to_a.sample}#{(1..9).to_a.sample.to_s.rjust(1, '0')}"
      while ToiletDefinition.find_by_identifier(id).present?
        id = "#{('A'..'Z').to_a.sample}#{(1..9).to_a.sample.to_s.rjust(1, '0')}"
      end
      id
    end
    points { 5 }
    instance
  end
end
