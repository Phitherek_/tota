FactoryBot.define do
  factory :instance do
    sequence(:name) { |n| "Test instance #{n}"}
    sequence(:description) { |n| "This is a long description. Very long. For a test instance. The test instance's number is #{n}."}
    activation_needed_qsos { 2 }
    activation_require_confirmation { true }
    trait :more_needed_qsos do
      activation_needed_qsos { 5 }
    end
    trait :no_confirmation do
      activation_require_confirmation { false }
    end
  end
end
