require 'rails_helper'

RSpec.describe LogEntry, type: :model do
  before(:each) do
    @td1 = FactoryBot.create(:toilet_definition)
    @td2 = FactoryBot.create(:toilet_definition, instance: @td1.instance)
  end

  it { is_expected.to belong_to :instance }
  it { is_expected.to belong_to :user }
  it { is_expected.to validate_presence_of :callsign_from }
  it { is_expected.to validate_presence_of :callsign_to }
  it { is_expected.to validate_presence_of :band }
  it { is_expected.to validate_presence_of :mode }
  it { is_expected.to validate_presence_of :qso_at }
  it { is_expected.to validate_presence_of :instance_id }
  it { is_expected.to validate_inclusion_of(:band).in_array(%w(160m 80m 60m 40m 30m 20m 17m 15m 12m 10m 6m 2m 70cm 23cm)) }
  it { is_expected.to validate_inclusion_of(:mode).in_array(['CW', 'SSB', 'FM', 'Dane', 'AM', 'Inna emisja']) }
  it 'validates callsign formats' do
    le = FactoryBot.build(:log_entry, :activation, instance: @td1.instance)
    le2 = FactoryBot.build(:log_entry, :activation, instance: @td1.instance, callsign_from: ('A'..'z').to_a.sample(10).join(''), callsign_to: ('A'..'z').to_a.sample(10).join(''))
    expect(le).to be_valid
    expect(le2).not_to be_valid
    expect(le2.errors[:callsign_from]).to include(I18n.t('errors.messages.invalid'))
    expect(le2.errors[:callsign_to]).to include(I18n.t('errors.messages.invalid'))
  end
  it 'checks if a toilet id is present' do
    le = FactoryBot.build(:log_entry)
    expect(le).not_to be_valid
    expect(le.errors[:base]).to include('Toaleta rozmówcy i/lub Twoja toaleta musi być wybrana!')
    le2 = FactoryBot.build(:log_entry, :activation, instance: @td1.instance)
    expect(le2).to be_valid
    le3 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance)
    expect(le3).to be_valid
    le4 = FactoryBot.build(:log_entry, :activation, :activation_ttt, instance: @td1.instance, toilet_id_from: @td1.identifier, toilet_id_to: @td2.identifier)
    expect(le4).to be_valid
  end
  it 'checks if toilet ids are not the same' do
    le = FactoryBot.build(:log_entry, :activation, :activation_ttt, instance: @td1.instance, toilet_id_from: @td1.identifier, toilet_id_to: @td1.identifier)
    expect(le).not_to be_valid
    expect(le.errors[:base]).to include('Toaleta rozmówcy i Twoja toaleta muszą mieć różne wartości!')
  end
  it 'checks if callsigns are not the same' do
    le = FactoryBot.build(:log_entry, :activation)
    le.callsign_from = le.callsign_to
    expect(le).not_to be_valid
    expect(le.errors[:base]).to include('Znak wywoławczy rozmówcy i Twój znak wywoławczy muszą mieć różne wartości!')
  end
  it 'checks for double activation' do
    le = FactoryBot.create(:log_entry, :activation, instance: @td1.instance)
    expect(le).to be_persisted
    le2 = FactoryBot.build(:log_entry, :activation, instance: @td1.instance, callsign_from: le.callsign_from, toilet_id_to: le.toilet_id_to, user: le.user)
    expect(le2).not_to be_valid
    expect(le2.errors[:base]).to include('Podwójne aktywacyjne QSO z tym samym znakiem nie jest możliwe!')
  end
  it 'checks for double activation - edge cases' do
    le = FactoryBot.create(:log_entry, :activation, instance: @td1.instance, qso_at: Time.now.utc.beginning_of_day)
    expect(le).to be_persisted
    le2 = FactoryBot.build(:log_entry, :activation, instance: @td1.instance, callsign_from: le.callsign_from, toilet_id_to: le.toilet_id_to, user: le.user, qso_at: Time.now.utc.end_of_day.beginning_of_minute)
    expect(le2).not_to be_valid
    expect(le2.errors[:base]).to include('Podwójne aktywacyjne QSO z tym samym znakiem nie jest możliwe!')
    le3 = FactoryBot.build(:log_entry, :activation, instance: @td1.instance, callsign_from: le.callsign_from, toilet_id_to: le.toilet_id_to, user: le.user, qso_at: Time.now.utc.end_of_day + 1.second)
    expect(le3).to be_valid
    le4 = FactoryBot.build(:log_entry, :activation, instance: @td1.instance, callsign_from: le.callsign_from, toilet_id_to: le.toilet_id_to, user: le.user, qso_at: Time.now.utc.beginning_of_day - 1.minute)
    expect(le4).to be_valid
  end
  it 'checks for double chase' do
    le = FactoryBot.create(:log_entry, :chase, instance: @td1.instance)
    expect(le).to be_persisted
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, callsign_from: le.callsign_from, toilet_id_from: le.toilet_id_from, user: le.user)
    expect(le2).not_to be_valid
    expect(le2.errors[:base]).to include('Podwójne łowy tej samej toalety z tym samym znakiem nie są możliwe!')
  end
  it 'checks for double chase - edge cases' do
    le = FactoryBot.create(:log_entry, :chase, instance: @td1.instance, qso_at: Time.now.utc.beginning_of_day)
    expect(le).to be_persisted
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, callsign_from: le.callsign_from, toilet_id_from: le.toilet_id_from, user: le.user, qso_at: Time.now.utc.end_of_day.beginning_of_minute)
    expect(le2).not_to be_valid
    expect(le2.errors[:base]).to include('Podwójne łowy tej samej toalety z tym samym znakiem nie są możliwe!')
    le3 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, callsign_from: le.callsign_from, toilet_id_from: le.toilet_id_from, user: le.user, qso_at: Time.now.utc.end_of_day + 1.second)
    expect(le3).to be_valid
    le4 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, callsign_from: le.callsign_from, toilet_id_from: le.toilet_id_from, user: le.user, qso_at: Time.now.utc.beginning_of_day - 1.minute)
    expect(le4).to be_valid
  end
  it 'checks toilet ids' do
    le = FactoryBot.build(:log_entry, :activation, instance: @td1.instance)
    expect(le).to be_valid
    le2 = FactoryBot.build(:log_entry, :activation)
    expect(le2).not_to be_valid
    expect(le2.errors[:toilet_id_to]).to include('musi mieć wartość ze zdefiniowanej listy')
    le3 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance)
    expect(le3).to be_valid
    le4 = FactoryBot.build(:log_entry, :chase)
    expect(le4).not_to be_valid
    expect(le4.errors[:toilet_id_from]).to include('musi mieć wartość ze zdefiniowanej listy')
    le5 = FactoryBot.build(:log_entry, :activation, :activation_ttt)
    expect(le5).not_to be_valid
    expect(le5.errors[:toilet_id_to]).to include('musi mieć wartość ze zdefiniowanej listy')
    expect(le5.errors[:toilet_id_from]).to include('musi mieć wartość ze zdefiniowanej listy')
  end

  it 'is not confirmed by default' do
    le = FactoryBot.create(:log_entry, :activation, instance: @td1.instance)
    expect(le).not_to be_confirmed
  end

  it 'confirms positive cross-validation examples' do
    le = FactoryBot.build(:log_entry, :activation, instance: @td1.instance)
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, qso_at: le.qso_at, toilet_id_from: le.toilet_id_to)
    le.callsign_from = le2.user.callsign
    le2.callsign_from = le.user.callsign
    le.save!
    le2.save!
    le.reload
    le2.reload
    expect(le).to be_confirmed
    expect(le2).to be_confirmed
    le = FactoryBot.build(:log_entry, :activation, instance: @td1.instance)
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, qso_at: le.qso_at + 1.minute, toilet_id_from: le.toilet_id_to)
    le.callsign_from = le2.user.callsign
    le2.callsign_from = le.user.callsign
    le.save!
    le2.save!
    le.reload
    le2.reload
    expect(le).to be_confirmed
    expect(le2).to be_confirmed
    le = FactoryBot.build(:log_entry, :activation, instance: @td1.instance)
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, qso_at: le.qso_at + 5.minutes, toilet_id_from: le.toilet_id_to)
    le.callsign_from = le2.user.callsign
    le2.callsign_from = le.user.callsign
    le.save!
    le2.save!
    le.reload
    le2.reload
    expect(le).to be_confirmed
    expect(le2).to be_confirmed
    le = FactoryBot.build(:log_entry, :activation, instance: @td1.instance)
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, qso_at: le.qso_at - 5.minutes, toilet_id_from: le.toilet_id_to)
    le.callsign_from = le2.user.callsign
    le2.callsign_from = le.user.callsign
    le.save!
    le2.save!
    le.reload
    le2.reload
    expect(le).to be_confirmed
    expect(le2).to be_confirmed
    le = FactoryBot.build(:log_entry, :activation, :activation_ttt, instance: @td1.instance, toilet_id_from: @td1.identifier, toilet_id_to: @td2.identifier)
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, toilet_id_from: le.toilet_id_to, toilet_id_to: le.toilet_id_from)
    le.callsign_from = le2.user.callsign
    le2.callsign_from = le.user.callsign
    le.save!
    le2.save!
    le.reload
    le2.reload
    expect(le).to be_confirmed
    expect(le2).to be_confirmed
  end

  it 'does not confirm negative cross-validation examples' do
    le = FactoryBot.build(:log_entry, :activation, instance: @td1.instance)
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, qso_at: le.qso_at + 6.minutes, toilet_id_from: le.toilet_id_to)
    le.callsign_from = le2.user.callsign
    le2.callsign_from = le.user.callsign
    le.save!
    le2.save!
    le.reload
    le2.reload
    expect(le).not_to be_confirmed
    expect(le2).not_to be_confirmed
    le = FactoryBot.build(:log_entry, :activation, instance: @td1.instance)
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, qso_at: le.qso_at - 6.minutes, toilet_id_from: le.toilet_id_to)
    le.callsign_from = le2.user.callsign
    le2.callsign_from = le.user.callsign
    le.save!
    le2.save!
    le.reload
    le2.reload
    expect(le).not_to be_confirmed
    expect(le2).not_to be_confirmed
    le = FactoryBot.build(:log_entry, :activation, instance: @td1.instance, toilet_id_to: @td1.identifier)
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, toilet_id_from: @td2.identifier)
    le.callsign_from = le2.user.callsign
    le2.callsign_from = le.user.callsign
    le.save!
    le2.save!
    le.reload
    le2.reload
    expect(le).not_to be_confirmed
    expect(le2).not_to be_confirmed
    le = FactoryBot.build(:log_entry, :activation, instance: @td1.instance)
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, qso_at: le.qso_at, toilet_id_from: le.toilet_id_to)
    le.save!
    le2.save!
    le.reload
    le2.reload
    expect(le).not_to be_confirmed
    expect(le2).not_to be_confirmed
    td3 = FactoryBot.create(:toilet_definition, instance: @td1.instance)
    td4 = FactoryBot.create(:toilet_definition, instance: @td1.instance)
    le = FactoryBot.build(:log_entry, :activation, :activation_ttt, instance: @td1.instance, toilet_id_from: @td1.identifier, toilet_id_to: @td2.identifier)
    le2 = FactoryBot.build(:log_entry, :chase, instance: @td1.instance, toilet_id_from: td3.identifier, toilet_id_to: td4.identifier)
    le.callsign_from = le2.user.callsign
    le2.callsign_from = le.user.callsign
    le.save!
    le2.save!
    le.reload
    le2.reload
    expect(le).not_to be_confirmed
    expect(le2).not_to be_confirmed
  end
end
