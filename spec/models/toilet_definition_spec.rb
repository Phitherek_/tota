require 'rails_helper'

RSpec.describe ToiletDefinition, type: :model do
  it { is_expected.to belong_to :instance }
  it { is_expected.to validate_presence_of :identifier }
  it { is_expected.to validate_uniqueness_of(:identifier).scoped_to(:instance_id) }
  it { is_expected.to validate_presence_of :points }
  it { is_expected.to validate_numericality_of(:points).is_greater_than(0).only_integer }
end
