require 'rails_helper'

RSpec.describe User, type: :model do
  it { is_expected.to have_many(:log_entries).dependent(:restrict_with_error) }
  it { is_expected.to have_many(:authentication_tokens).dependent(:destroy) }
  it { is_expected.to validate_presence_of :login }
  it { is_expected.to validate_presence_of :callsign }
  it 'validates callsign format' do
    u = FactoryBot.build(:user)
    expect(u).to be_valid
    u2 = FactoryBot.build(:user, callsign: ('A'..'z').to_a.sample(10).join(''))
    expect(u2).not_to be_valid
    expect(u2.errors[:callsign]).to include(I18n.t('errors.messages.invalid'))
  end
  it 'upcases callsign and removes breaking' do
    u = FactoryBot.build(:user, callsign: 'so9ph/p/m')
    expect(u).to be_valid
    expect(u.callsign).to eq('SO9PH')
  end
  it 'validates login uniqueness' do
    u = FactoryBot.create(:user)
    u2 = FactoryBot.build(:user)
    u2.login = u.login
    u3 = FactoryBot.build(:user)
    expect(u2).not_to be_valid
    expect(u2.errors[:login]).to include(I18n.t('errors.messages.taken'))
    expect(u3).to be_valid
  end
  it 'validates callsign uniqueness' do
    u = FactoryBot.create(:user)
    u2 = FactoryBot.build(:user)
    u2.callsign = u.callsign
    u3 = FactoryBot.build(:user)
    expect(u2).not_to be_valid
    expect(u2.errors[:callsign]).to include(I18n.t('errors.messages.taken'))
    expect(u3).to be_valid
  end
  it 'has working filter scope' do
    u = FactoryBot.create(:user)
    expect(User.filter('test')).to include(u)
    expect(User.filter(u.callsign.first(2))).to include(u)
    expect(User.filter('eqewe')).not_to include(u)
  end
end
