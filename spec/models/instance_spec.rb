require 'rails_helper'

RSpec.describe Instance, type: :model do
  it { is_expected.to have_many(:authentication_tokens).dependent(:destroy) }
  it { is_expected.to have_many(:log_entries).dependent(:destroy) }
  it { is_expected.to have_many(:toilet_definitions).dependent(:destroy) }
  it { is_expected.to validate_uniqueness_of :ident_str }
  it { is_expected.to validate_presence_of :name }
  it 'autogenerates ident_str' do
    i = FactoryBot.build(:instance)
    expect(i).to be_valid
    expect(i.ident_str).to be_present
  end
  it 'has working filter scope' do
    i = FactoryBot.create(:instance)
    expect(Instance.filter('test')).to include(i)
    expect(Instance.filter('zefsd')).not_to include(i)
  end
end
