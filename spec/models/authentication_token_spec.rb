require 'rails_helper'

RSpec.describe AuthenticationToken, type: :model do
  it { is_expected.to belong_to :user }
  it { is_expected.to belong_to :instance }
  it { is_expected.to validate_uniqueness_of :token }
  it 'autogenerates token' do
    t = FactoryBot.build(:authentication_token)
    expect(t).to be_valid
    expect(t.token).to be_present
  end
end
