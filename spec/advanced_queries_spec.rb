require 'rails_helper'

RSpec.describe Tota::AdvancedQueries do
  it 'correctly implements activation_query method' do
    instance = FactoryBot.create(:instance)
    query1 = <<SQL
SELECT DISTINCT ON(log_entries.user_id, log_entries.callsign_to, log_entries.toilet_id_to, date_trunc('day', log_entries.qso_at)) log_entries.user_id, log_entries.callsign_to, log_entries.toilet_id_to, date_trunc('day', log_entries.qso_at) AS qso_at, toilet_definitions.points, COUNT(ac.*) >= #{instance.activation_needed_qsos} AS confirmed FROM "log_entries" LEFT OUTER JOIN "toilet_definitions" ON log_entries.toilet_id_to = toilet_definitions.identifier LEFT OUTER JOIN (SELECT callsign_from, toilet_id_from, qso_at, confirmed FROM log_entries AS le) ac ON ac.toilet_id_from = log_entries.toilet_id_to AND ac.callsign_from = log_entries.callsign_to AND ac.qso_at >= date_trunc('day', log_entries.qso_at) AND ac.qso_at <= date_trunc('day', log_entries.qso_at) + interval '1 day' - interval '1 second'#{instance.activation_require_confirmation ? ' AND ac.confirmed = TRUE' : ''} WHERE log_entries.toilet_id_to <> '' AND "log_entries"."instance_id" = #{instance.id} GROUP BY log_entries.user_id, log_entries.callsign_to, log_entries.toilet_id_to, log_entries.qso_at, toilet_definitions.points ORDER BY date_trunc('day', log_entries.qso_at) DESC
SQL
    user = FactoryBot.create(:user)
    query2 = <<SQL
SELECT DISTINCT ON(log_entries.user_id, log_entries.callsign_to, log_entries.toilet_id_to, date_trunc('day', log_entries.qso_at)) log_entries.user_id, log_entries.callsign_to, log_entries.toilet_id_to, date_trunc('day', log_entries.qso_at) AS qso_at, toilet_definitions.points, COUNT(ac.*) >= #{instance.activation_needed_qsos} AS confirmed FROM "log_entries" LEFT OUTER JOIN "toilet_definitions" ON log_entries.toilet_id_to = toilet_definitions.identifier LEFT OUTER JOIN (SELECT callsign_from, toilet_id_from, qso_at, confirmed FROM log_entries AS le) ac ON ac.toilet_id_from = log_entries.toilet_id_to AND ac.callsign_from = log_entries.callsign_to AND ac.qso_at >= date_trunc('day', log_entries.qso_at) AND ac.qso_at <= date_trunc('day', log_entries.qso_at) + interval '1 day' - interval '1 second'#{instance.activation_require_confirmation ? ' AND ac.confirmed = TRUE' : ''} WHERE log_entries.toilet_id_to <> '' AND "log_entries"."instance_id" = #{instance.id}#{user.present? ? " AND log_entries.user_id = #{user.id}" : ''} GROUP BY log_entries.user_id, log_entries.callsign_to, log_entries.toilet_id_to, log_entries.qso_at, toilet_definitions.points ORDER BY date_trunc('day', log_entries.qso_at) DESC
SQL
    expect(Tota::AdvancedQueries.activations_query(instance)).to eq(query1)
    expect(Tota::AdvancedQueries.activations_query(instance, user)).to eq(query2)
  end

  it 'gives appropriate responses from activation query', :postgres do
    instance = FactoryBot.create(:instance)
    user = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user)
    user3 = FactoryBot.create(:user)
    td1 = FactoryBot.create(:toilet_definition, instance: instance)
    td2 = FactoryBot.create(:toilet_definition, instance: instance)
    le1 = FactoryBot.create(:log_entry, :activation, user: user, instance: instance, callsign_from: user2.callsign, toilet_id_to: td1.identifier)
    FactoryBot.create(:log_entry, :chase, user: user2, instance: instance, callsign_from: user.callsign, toilet_id_from: td1.identifier)
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.activations_query(instance)).to_a).to eq([{'user_id' => user.id, 'callsign_to' => user.callsign, 'toilet_id_to' => td1.identifier, 'qso_at' => le1.qso_at.beginning_of_day.strftime('%Y-%m-%d %H:%M:%S'), 'points' => td1.points, 'confirmed' => false}])
    FactoryBot.create(:log_entry, :chase, user: user3, instance: instance, callsign_from: user.callsign, toilet_id_from: td1.identifier)
    FactoryBot.create(:log_entry, :activation, user: user, instance: instance, callsign_from: user3.callsign, toilet_id_to: td1.identifier)
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.activations_query(instance)).to_a).to eq([{'user_id' => user.id, 'callsign_to' => user.callsign, 'toilet_id_to' => td1.identifier, 'qso_at' => le1.qso_at.beginning_of_day.strftime('%Y-%m-%d %H:%M:%S'), 'points' => td1.points, 'confirmed' => true}])
    le3 = FactoryBot.create(:log_entry, :activation, user: user2, instance: instance, callsign_from: user3.callsign, toilet_id_to: td2.identifier)
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.activations_query(instance)).to_a).to eq([{'user_id' => user.id, 'callsign_to' => user.callsign, 'toilet_id_to' => td1.identifier, 'qso_at' => le1.qso_at.beginning_of_day.strftime('%Y-%m-%d %H:%M:%S'), 'points' => td1.points, 'confirmed' => true}, {'user_id' => user2.id, 'callsign_to' => user2.callsign, 'toilet_id_to' => td2.identifier, 'qso_at' => le3.qso_at.beginning_of_day.strftime('%Y-%m-%d %H:%M:%S'), 'points' => td2.points, 'confirmed' => false}])
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.activations_query(instance, user)).to_a).to eq([{'user_id' => user.id, 'callsign_to' => user.callsign, 'toilet_id_to' => td1.identifier, 'qso_at' => le1.qso_at.beginning_of_day.strftime('%Y-%m-%d %H:%M:%S'), 'points' => td1.points, 'confirmed' => true}])
  end

  it 'correctly implements ranking_query method' do
    instance = FactoryBot.create(:instance)
    query1 = <<SQL
SELECT * FROM (SELECT ranking.user_id, ranking.callsign, ranking.activator_points, ranking.chaser_points, ranking.activator_points + ranking.chaser_points AS total_points FROM (SELECT users.id AS user_id, users.callsign, COALESCE(SUM(activations.points), 0) AS activator_points, COALESCE(COUNT(chases.*), 0) AS chaser_points FROM users LEFT OUTER JOIN (#{Tota::AdvancedQueries.activations_query(instance)}) activations ON activations.user_id = users.id AND (activations.confirmed = TRUE OR activations.confirmed IS NULL) LEFT OUTER JOIN (SELECT user_id, callsign_from FROM log_entries WHERE log_entries.toilet_id_from IS NOT NULL AND log_entries.instance_id = #{instance.id}) chases ON chases.user_id = users.id GROUP BY users.callsign, users.id) ranking) inner_ranking ORDER BY inner_ranking.total_points DESC, inner_ranking.activator_points DESC, inner_ranking.chaser_points DESC, inner_ranking.callsign ASC
SQL
    user = FactoryBot.create(:user)
    query2 = <<SQL
SELECT * FROM (SELECT ranking.user_id, ranking.callsign, ranking.activator_points, ranking.chaser_points, ranking.activator_points + ranking.chaser_points AS total_points FROM (SELECT users.id AS user_id, users.callsign, COALESCE(SUM(activations.points), 0) AS activator_points, COALESCE(COUNT(chases.*), 0) AS chaser_points FROM users LEFT OUTER JOIN (#{Tota::AdvancedQueries.activations_query(instance, user)}) activations ON activations.user_id = users.id AND (activations.confirmed = TRUE OR activations.confirmed IS NULL) LEFT OUTER JOIN (SELECT user_id, callsign_from FROM log_entries WHERE log_entries.toilet_id_from IS NOT NULL AND log_entries.instance_id = #{instance.id}) chases ON chases.user_id = users.id GROUP BY users.callsign, users.id) ranking#{user.present? ? " WHERE ranking.user_id=#{user.id}" : ''}) inner_ranking ORDER BY inner_ranking.total_points DESC, inner_ranking.activator_points DESC, inner_ranking.chaser_points DESC, inner_ranking.callsign ASC
SQL
    query3 = <<SQL
SELECT * FROM (SELECT ranking.user_id, ranking.callsign, ranking.activator_points, ranking.chaser_points, ranking.activator_points + ranking.chaser_points AS total_points FROM (SELECT users.id AS user_id, users.callsign, COALESCE(SUM(activations.points), 0) AS activator_points, COALESCE(COUNT(chases.*), 0) AS chaser_points FROM users LEFT OUTER JOIN (#{Tota::AdvancedQueries.activations_query(instance, user)}) activations ON activations.user_id = users.id AND (activations.confirmed = TRUE OR activations.confirmed IS NULL) LEFT OUTER JOIN (SELECT user_id, callsign_from FROM log_entries WHERE log_entries.toilet_id_from IS NOT NULL AND log_entries.instance_id = #{instance.id}) chases ON chases.user_id = users.id GROUP BY users.callsign, users.id) ranking#{user.present? ? " WHERE ranking.user_id=#{user.id}" : ''}) inner_ranking ORDER BY inner_ranking.total_points DESC, inner_ranking.activator_points DESC, inner_ranking.chaser_points DESC, inner_ranking.callsign ASC LIMIT 10
SQL
    expect(Tota::AdvancedQueries.ranking_query(instance)).to eq(query1)
    expect(Tota::AdvancedQueries.ranking_query(instance, user)).to eq(query2)
    expect(Tota::AdvancedQueries.ranking_query(instance, user, 10)).to eq(query3)
  end

  it 'gives appropriate responses from ranking query', :postgres do
    instance = FactoryBot.create(:instance)
    user = FactoryBot.create(:user)
    user2 = FactoryBot.create(:user)
    user3 = FactoryBot.create(:user)
    user4 = FactoryBot.create(:user)
    user5 = FactoryBot.create(:user)
    user6 = FactoryBot.create(:user)
    users = [user, user2, user3, user4, user5, user6].sort_by! { |u| u.callsign }
    td1 = FactoryBot.create(:toilet_definition, instance: instance)
    td2 = FactoryBot.create(:toilet_definition, instance: instance)
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.ranking_query(instance)).to_a).to eq(users.collect { |u| {'user_id' => u.id, 'callsign' => u.callsign, 'activator_points' => 0, 'chaser_points' => 0, 'total_points' => 0}})
    FactoryBot.create(:log_entry, :activation, user: user3, instance: instance, callsign_from: user.callsign, toilet_id_to: td1.identifier)
    FactoryBot.create(:log_entry, :chase, user: user, instance: instance, callsign_from: user3.callsign, toilet_id_from: td1.identifier)
    FactoryBot.create(:log_entry, :activation, user: user3, instance: instance, callsign_from: user5.callsign, toilet_id_to: td1.identifier)
    FactoryBot.create(:log_entry, :chase, user: user5, instance: instance, callsign_from: user3.callsign, toilet_id_from: td1.identifier)
    users = [user3] + [user, user5].sort_by! { |u| u.callsign } + [user2, user4, user6].sort_by! { |u| u.callsign }
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.ranking_query(instance)).to_a).to eq(users.collect do |u|
      activator_points = 0
      activator_points = td1.points if u == user3
      chaser_points = 0
      chaser_points = 1 if u == user || u == user5
      total_points = activator_points + chaser_points
      {'user_id' => u.id, 'callsign' => u.callsign, 'activator_points' => activator_points, 'chaser_points' => chaser_points, 'total_points' => total_points}
    end)
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.ranking_query(instance, nil, 3)).count).to eq(3)
    users = [user3] + [user, user5].sort_by! { |u| u.callsign }
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.ranking_query(instance, nil, 3)).to_a).to eq(users.collect do |u|
      activator_points = 0
      activator_points = td1.points if u == user3
      chaser_points = 0
      chaser_points = 1 if u == user || u == user5
      total_points = activator_points + chaser_points
      {'user_id' => u.id, 'callsign' => u.callsign, 'activator_points' => activator_points, 'chaser_points' => chaser_points, 'total_points' => total_points}
    end)
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.ranking_query(instance, user3)).count).to eq(1)
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.ranking_query(instance, user3)).to_a).to eq([{'user_id' => user3.id, 'callsign' => user3.callsign, 'activator_points' => td1.points, 'chaser_points' => 0, 'total_points' => td1.points}])
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.ranking_query(instance, user)).to_a).to eq([{'user_id' => user.id, 'callsign' => user.callsign, 'activator_points' => 0, 'chaser_points' => 1, 'total_points' => 1}])
    FactoryBot.create(:log_entry, :activation, user: user, instance: instance, callsign_from: user5.callsign, toilet_id_to: td2.identifier)
    FactoryBot.create(:log_entry, :activation, user: user2, instance: instance, callsign_from: user5.callsign, toilet_id_to: td2.identifier)
    FactoryBot.create(:log_entry, :activation, user: user4, instance: instance, callsign_from: user5.callsign, toilet_id_to: td2.identifier)
    FactoryBot.create(:log_entry, :activation, user: user6, instance: instance, callsign_from: user5.callsign, toilet_id_to: td2.identifier)
    FactoryBot.create(:log_entry, :chase, user: user5, instance: instance, callsign_from: user.callsign, toilet_id_from: td2.identifier)
    FactoryBot.create(:log_entry, :chase, user: user5, instance: instance, callsign_from: user2.callsign, toilet_id_from: td2.identifier)
    FactoryBot.create(:log_entry, :chase, user: user5, instance: instance, callsign_from: user4.callsign, toilet_id_from: td2.identifier)
    FactoryBot.create(:log_entry, :chase, user: user5, instance: instance, callsign_from: user6.callsign, toilet_id_from: td2.identifier)
    users = [user3, user5, user] + [user2, user4, user6].sort_by! { |u| u.callsign }
    expect(ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.ranking_query(instance)).to_a).to eq(users.collect do |u|
      activator_points = 0
      activator_points = td1.points if u == user3
      chaser_points = 0
      chaser_points = 1 if u == user
      chaser_points = 5 if u == user5
      total_points = activator_points + chaser_points
      {'user_id' => u.id, 'callsign' => u.callsign, 'activator_points' => activator_points, 'chaser_points' => chaser_points, 'total_points' => total_points}
    end)
  end
end