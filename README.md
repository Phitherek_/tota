# TOTA

TOTA logging system - backend and admin panel

* Ruby version: 2.5.1

* System dependencies: postgresql

* Database creation: rake db:create, rake db:migrate

* Unit tests: rake spec

* Unit tests without PostgreSQL specific tests: rspec --tag ~@postgres