class InstancesController < ApplicationController
  before_action :authenticate_user!
  before_action :ensure_admin
  before_action :find_instance, only: [:update, :destroy]

  def index
    @instances = Instance.all
    @instances = @instances.filter(params[:filter]) if params[:filter].present?
    @instances = @instances.order(:created_at)
    @new_instance = Instance.new
  end

  def create
    @instance = Instance.create(instance_params)
    if @instance.persisted?
      flash[:success] = 'Instancja dodana pomyślnie!'
    else
      flash[:error] = "Błąd przy dodawaniu instancji: #{@instance.errors.full_messages.join(',')}"
    end
    redirect_to instances_url
  end

  def update
    if @instance.update(instance_params)
      flash[:success] = 'Instancja zaktualizowana pomyślnie!'
    else
      flash[:error] = "Błąd przy aktualizacji instancji: #{@instance.errors.full_messages.join(',')}"
    end
    redirect_to instances_url
  end

  def destroy
    if @instance.destroy
      flash[:success] = 'Instancja usunięta pomyślnie!'
    else
      flash[:error] = "Błąd przy usuwaniu instancji: #{@instance.errors.full_messages.join(',')}"
    end
    redirect_to instances_url
  end

  private

  def find_instance
    @instance = Instance.find(params[:id])
  end

  def instance_params
    params.require(:instance).permit(:name, :description, :activation_needed_qsos, :activation_require_confirmation)
  end
end
