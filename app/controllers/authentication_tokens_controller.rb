class AuthenticationTokensController < ApplicationController
  before_action :authenticate_user!
  before_action :ensure_admin
  def index
    @authentication_tokens = AuthenticationToken.all
    @authentication_tokens = @authentication_tokens.where(user_id: params[:user_id]) if params[:user_id].present?
    @authentication_tokens = @authentication_tokens.where(instance_id: params[:instance_id]) if params[:instance_id].present?
    @authentication_tokens = @authentication_tokens.order(instance_id: :asc, user_id: :asc, created_at: :desc)
    @instances = Instance.order(name: :asc)
    @users = User.order(login: :asc)
  end

  def destroy
    @authentication_token = AuthenticationToken.find(params[:id])
    if @authentication_token.destroy
      flash[:success] = 'Token autoryzacji usunięty pomyślnie!'
    else
      flash[:error] = "Błąd przy usuwaniu tokenu autoryzacji: #{@authentication_token.errors.full_messages.join(', ')}"
    end
  end
end
