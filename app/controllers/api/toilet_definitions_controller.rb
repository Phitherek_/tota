class Api::ToiletDefinitionsController < ApiController
  before_action :find_instance

  def index
    toilet_definitions = @instance.toilet_definitions.order(identifier: :asc)
    render json: ToiletDefinitionSerializer.new(toilet_definitions).serializable_hash
  end
end
