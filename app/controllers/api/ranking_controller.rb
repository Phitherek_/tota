class Api::RankingController < ApiController
  before_action :authorize_with_auth_header, except: :top
  before_action :find_instance, only: :top

  def top
    limit = params[:limit] || 10
    ranking_entries = ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.ranking_query(@instance, nil, limit)).to_a
    render json: { ranking_entries: ranking_entries }
  end

  def my
    ranking_entry = ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.ranking_query(@instance, @current_user)).to_a.first
    render json: { ranking_entry: ranking_entry }
  end
end
