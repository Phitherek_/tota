class Api::LogEntriesController < ApiController
  before_action :authorize_with_auth_header, except: :top
  before_action :find_instance, only: :top
  def top
    log_entries = @instance.log_entries.order(qso_at: :desc).limit(params[:limit])
    render json: LogEntrySerializer.new(log_entries).serializable_hash
  end

  def index
    log_entries = @current_user.log_entries.where(instance: @instance).order(qso_at: :desc)
    log_entries = log_entries.limit(params[:limit]) if params[:limit]
    render json: LogEntrySerializer.new(log_entries).serializable_hash
  end

  def activations
    activations = ActiveRecord::Base.connection.exec_query(Tota::AdvancedQueries.activations_query(@instance, @current_user)).to_a
    render json: { activations: activations }
  end

  def create
    log_entry = @current_user.log_entries.create(instance: @instance, callsign_from: params[:callsign_from], toilet_id_from: params[:toilet_id_from], callsign_to: params[:callsign_to], toilet_id_to: params[:toilet_id_to], band: params[:band], mode: params[:mode], qso_at: params[:qso_at])
    if log_entry.persisted?
      render json: { success: 'success', object: LogEntrySerializer.new(log_entry).serializable_hash }
    else
      render status: 422, json: { error: 'log_entry_validation', details: log_entry.errors.to_hash }
    end
  end

  def show
    log_entry = @current_user.log_entries.where(instance: @instance).find_by_id(params[:id])
    if log_entry.blank?
      render status: 404, json: { error: 'log_entry_not_found' } and return
    end
    render json: LogEntrySerializer.new(log_entry).serializable_hash
  end

  def update
    log_entry = LogEntry.find_by(user: @current_user, instance: @instance, id: params[:id])
    if log_entry.blank?
      render status: 404, json: { error: 'log_entry_not_found' } and return
    end
    if log_entry.update(callsign_from: params[:callsign_from], toilet_id_from: params[:toilet_id_from], callsign_to: params[:callsign_to], toilet_id_to: params[:toilet_id_to], band: params[:band], mode: params[:mode], qso_at: params[:qso_at])
      render json: { success: 'success', object: LogEntrySerializer.new(log_entry).serializable_hash }
    else
      render status: 422, json: { error: 'log_entry_validation', details: log_entry.errors.to_hash }
    end
  end

  def destroy
    log_entry = LogEntry.find_by(user: @current_user, instance: @instance, id: params[:id])
    if log_entry.blank?
      render status: 404, json: { error: 'log_entry_not_found' } and return
    end
    if log_entry.destroy
      render json: { success: 'success' }
    else
      render status: 422, json: { error: 'log_entry_validation', details: log_entry.errors.to_hash }
    end
  end
end
