class Api::UsersController < ApiController
  before_action :authorize_with_auth_header, except: [:register, :login]

  def me
    render json: UserSerializer.new(@current_user).serializable_hash
  end

  def profile
    unless @current_user.valid_password?(params[:current_password])
      render status: 403, json: { error: 'invalid_auth' } and return
    end
    if @current_user.update(callsign: params[:callsign], password: params[:password], password_confirmation: params[:password_confirmation])
      render json: { success: 'success', object: UserSerializer.new(@current_user).serializable_hash }
    else
      render status: 422, json: { error: 'user_validation', datails: @current_user.errors.to_hash }
    end
  end

  def register
    @user = User.create(login: params[:login], callsign: params[:callsign], password: params[:password], password_confirmation: params[:password_confirmation])
    if @user.persisted?
      render json: { success: 'success' }
    else
      render status: 422, json: { error: 'user_validation', details: @user.errors.to_hash }
    end
  end

  def login
    @instance = Instance.find_by_ident_str(params[:instance_key])
    if @instance.blank?
      render status: 404, json: { error: 'no_instance' } and return
    end
    @user = User.find_by_login(params[:login].downcase)
    if @user.blank?
      render status: 403, json: { error: 'invalid_auth' } and return
    end
    unless @user.valid_password?(params[:password])
      render status: 403, json: { error: 'invalid_auth' } and return
    end
    @token = @instance.authentication_tokens.create(user: @user)
    if @token.persisted?
      render json: { token: @token.token + @instance.ident_str }
    else
      render status: 422, json: { error: 'token_validation', details: @token.errors.to_hash }
    end
  end

  def logout
    if @token.destroy
      render json: { success: 'success' }
    else
      render status: 422, json: { error: 'token_validation', details: @token.errors.to_hash }
    end
  end
end
