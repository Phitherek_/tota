class ApiController < ApplicationController
  protect_from_forgery with: :null_session

  protected

  def find_instance
    @instance = Instance.find_by_ident_str(params[:instance_key])
    if @instance.blank?
      render status: 403, json: { error: 'invalid_instance'} and return
    end
  end

  def authorize_with_auth_header
    auth = request.headers['Authorization']
    if auth.blank?
      render status: 403, json: { error: 'missing_auth_header' } and return
    end
    if auth.split(' ')[0] != 'Token'
      render status: 403, json: { error: 'invalid_auth' } and return
    end
    joined_auth_data = auth.split(' ')[1]
    @token = AuthenticationToken.find_by_token(joined_auth_data[0..127])
    if @token.blank?
      render status: 403, json: { error: 'invalid_auth' } and return
    end
    @current_user = @token.user
    @instance = Instance.find_by_ident_str(joined_auth_data[128..147])
    if @instance.blank?
      render status: 403, json: { error: 'invalid_auth' } and return
    end
  end
end
