require 'date'
class LogEntriesController < ApplicationController
  before_action :authenticate_user!
  before_action :ensure_admin
  before_action :find_log_entry, only: [:update, :destroy]
  def index
    @log_entries = LogEntry.all
    @log_entries = @log_entries.where(instance_id: params[:instance_id]) if params[:instance_id].present?
    @log_entries = @log_entries.where(user_id: params[:user_id]) if params[:user_id].present?
    @log_entries = @log_entries.order(instance_id: :asc, user_id: :asc, qso_at: :desc)
    @instances = Instance.order(name: :asc)
    @users = User.order(login: :asc)
  end

  def update
    if @log_entry.update(log_entry_params)
      flash[:success] = 'QSO zaktualizowane pomyślnie!'
    else
      flash[:error] = "Błąd przy aktualizacji QSO: #{@log_entry.errors.full_messages.join(',')}"
    end
    redirect_to log_entries_url
  end

  def destroy
    if @log_entry.destroy
      flash[:success] = 'QSO usunięte pomyślnie!'
    else
      flash[:error] = "Błąd przy usuwaniu QSO: #{@log_entry.errors.full_messages.join(',')}"
    end
    redirect_to log_entries_url
  end

  private

  def find_log_entry
    @log_entry = LogEntry.find(params[:id])
  end

  def log_entry_params
    params.require(:log_entry).permit(:callsign_from, :toilet_id_from, :callsign_to, :toilet_id_to, :band, :mode, :qso_at)
  end
end
