class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:callsign])
    devise_parameter_sanitizer.permit(:account_update, keys: [:callsign])
  end

  def ensure_admin
    if user_signed_in? && !current_user.admin?
      flash[:error] = 'Musisz mieć uprawnienia administratora'
      sign_out current_user
      redirect_to new_user_session_url
    end
  end
end
