class ToiletDefinitionsController < ApplicationController
  before_action :authenticate_user!
  before_action :ensure_admin
  before_action :find_toilet_definition, only: [:update, :destroy]

  def index
    @toilet_definitions = ToiletDefinition.all
    @toilet_definitions = @toilet_definitions.where(instance: Instance.find(params[:instance_id])) if params[:instance_id].present?
    @toilet_definitions = @toilet_definitions.order(instance_id: :asc, created_at: :desc)
    @new_toilet_definition = ToiletDefinition.new
    @instances = Instance.order(name: :asc)
  end

  def create
    @toilet_definition = ToiletDefinition.create(toilet_definition_params)
    if @toilet_definition.persisted?
      flash[:success] = 'Definicja toalety dodana pomyślnie!'
    else
      flash[:error] = "Błąd przy dodawaniu definicji toalety: #{@toilet_definition.errors.full_messages.join(', ')}"
    end
    redirect_to toilet_definitions_url
  end

  def update
    if @toilet_definition.update(toilet_definition_params)
      flash[:success] = 'Definicja toalety zaktualizowana pomyślnie!'
    else
      flash[:error] = "Błąd przy aktualizacji definicji toalety: #{@toilet_definition.errors.full_messages.join(', ')}"
    end
    redirect_to toilet_definitions_url
  end

  def destroy
    if @toilet_definition.destroy
      flash[:success] = 'Definicja toalety usunięta pomyślnie!'
    else
      flash[:error] = "Błąd przy usuwaniu definicji toalety: #{@toilet_definition.errors.full_messages.join(', ')}"
    end
    redirect_to toilet_definitions_url
  end

  private

  def find_toilet_definition
    @toilet_definition = ToiletDefinition.find(params[:id])
  end

  def toilet_definition_params
    params.require(:toilet_definition).permit(:instance_id, :identifier, :points)
  end
end
