class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :ensure_admin
  before_action :find_user, only: [:update, :destroy]
  def index
    @users = User.all
    @users = @users.filter(params[:filter]) if params[:filter].present?
    @users = @users.order(admin: :desc, created_at: :desc)
    @new_user = User.new
  end

  def create
    @user = User.create(user_params)
    if @user.persisted?
      flash[:success] = 'Użytkownik stworzony pomyślnie!'
    else
      flash[:error] = "Błąd przy tworzeniu użytkownika: #{@user.errors.full_messages.join(', ')}"
    end
    redirect_to users_url
  end

  def update
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end
    if @user.update(user_params)
      flash[:success] = 'Użytkownik zaktualizowany pomyślnie!'
    else
      flash[:error] = "Błąd przy aktualizacji użytkownika: #{@user.errors.full_messages.join(', ')}"
    end
    redirect_to users_url
  end

  def destroy
    if @user.destroy
      flash[:success] = 'Użytkownik usunięty pomyślnie!'
    else
      flash[:error] = "Błąd przy usuwaniu użytkownika: #{@user.errors.full_messages.join(', ')}"
    end
    redirect_to users_url
  end

  private

  def find_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:login, :callsign, :admin, :password, :password_confirmation)
  end
end
