class LogEntry < ApplicationRecord
  belongs_to :user
  belongs_to :instance
  validates :callsign_from, presence: true, format: /([A-Z0-9]+\/)?(([0-9][A-Z][0-9][A-Z]{3})|([A-Z][0-9][A-Z]{1,3})|([A-Z]{2}[0-9][A-Z]{1,3}))((\/[A-z0-9]+)+)?/
  validates :callsign_to, presence: true, format: /([A-Z0-9]+\/)?(([0-9][A-Z][0-9][A-Z]{3})|([A-Z][0-9][A-Z]{1,3})|([A-Z]{2}[0-9][A-Z]{1,3}))((\/[A-z0-9]+)+)?/
  validates :band, presence: true, inclusion: { in: %w(160m 80m 60m 40m 30m 20m 17m 15m 12m 10m 6m 2m 70cm 23cm) }
  validates :mode, presence: true, inclusion: { in: ['CW', 'SSB', 'FM', 'Dane', 'AM', 'Inna emisja'] }
  validates :qso_at, presence: true
  validates :instance_id, presence: true
  validate :check_if_toilet_present, :check_if_not_the_same_toilet, :check_if_not_the_same_callsign, :check_for_double_chase, :check_for_double_activation_qso, :check_toilet_ids
  after_save :update_entry_confirmation


  private

  def check_if_toilet_present
    if toilet_id_from.blank? && toilet_id_to.blank?
      errors.add(:base, 'Toaleta rozmówcy i/lub Twoja toaleta musi być wybrana!')
    end
  end

  def check_if_not_the_same_toilet
    if toilet_id_from == toilet_id_to
      errors.add(:base, 'Toaleta rozmówcy i Twoja toaleta muszą mieć różne wartości!')
    end
  end

  def check_if_not_the_same_callsign
    if callsign_from == callsign_to
      errors.add(:base, "Znak wywoławczy rozmówcy i Twój znak wywoławczy muszą mieć różne wartości!")
    end
  end

  def check_for_double_activation_qso
    if toilet_id_to.present? && user.log_entries.where.not(id: id).where(toilet_id_to: toilet_id_to, callsign_from: callsign_from).where('qso_at >= ? AND qso_at <= ?', qso_at.to_time.utc.beginning_of_day, qso_at.to_time.utc.end_of_day).any?
      errors.add(:base, "Podwójne aktywacyjne QSO z tym samym znakiem nie jest możliwe!")
    end
  end

  def check_for_double_chase
    if toilet_id_from.present? && user.log_entries.where.not(id: id).where(toilet_id_from: toilet_id_from, callsign_from: callsign_from).where('qso_at >= ? AND qso_at <= ?', qso_at.to_time.utc.beginning_of_day, qso_at.to_time.utc.end_of_day).any?
      errors.add(:base, "Podwójne łowy tej samej toalety z tym samym znakiem nie są możliwe!")
    end
  end

  def update_entry_confirmation
    entry = self.class.where(callsign_from: self.callsign_to, callsign_to: self.callsign_from).where('toilet_id_from = ? OR toilet_id_to = ?', self.toilet_id_to, self.toilet_id_from).where('qso_at >= ? AND qso_at <= ?', self.qso_at - 5.minutes, self.qso_at + 5.minutes).first
    if entry.present?
      self.update_column(:confirmed, true)
      entry.update_column(:confirmed, true)
    else
      self.update_column(:confirmed, false)
    end
  end

  def check_toilet_ids
    if instance.present?
      toilet_ids = instance.toilet_definitions.pluck(:identifier)
      if toilet_id_from.present? && toilet_ids.exclude?(toilet_id_from)
        errors.add(:toilet_id_from, "musi mieć wartość ze zdefiniowanej listy")
      end
      if toilet_id_to.present? && toilet_ids.exclude?(toilet_id_to)
        errors.add(:toilet_id_to, "musi mieć wartość ze zdefiniowanej listy")
      end
    end
  end
end
