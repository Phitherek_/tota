require 'securerandom'
class AuthenticationToken < ApplicationRecord
  belongs_to :user
  belongs_to :instance
  validates :token, presence: true, uniqueness: true
  before_validation :generate_token

  private

  def generate_token
    if self.token.blank?
      token = SecureRandom.hex(64)
      while self.class.where(token: token).any?
        token = SecureRandom.hex(64)
      end
      self.token = token
    end
  end
end
