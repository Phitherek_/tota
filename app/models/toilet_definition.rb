class ToiletDefinition < ApplicationRecord
  belongs_to :instance
  validates :identifier, presence: true, uniqueness: { scope: :instance_id }
  validates :points, presence: true, numericality: { greater_than: 0, only_integer: true }
end
