class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :rememberable, :validatable
  has_many :log_entries, dependent: :restrict_with_error
  has_many :authentication_tokens, dependent: :destroy
  validates :login, presence: true, uniqueness: true
  validates :callsign, presence: true, uniqueness: true, format: /([0-9][A-Z][0-9][A-Z]{3})|([A-Z][0-9][A-Z]{1,3})|([A-Z]{2}[0-9][A-Z]{1,3})/
  before_validation :upcase_callsign
  scope :filter, -> (filter) { where('login LIKE LOWER(?) OR callsign LIKE UPPER(?)', "%#{filter}%", "%#{filter}%") }

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def will_save_change_to_email?
    false
  end

  private

  def upcase_callsign
    if callsign.present?
      self.callsign = callsign.split('/')[0].upcase
    end
  end
end
