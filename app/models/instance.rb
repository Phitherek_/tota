require 'securerandom'
class Instance < ApplicationRecord
  has_many :authentication_tokens, dependent: :destroy
  has_many :log_entries, dependent: :destroy
  has_many :toilet_definitions, dependent: :destroy
  validates :ident_str, presence: true, uniqueness: true
  validates :name, presence: true
  before_validation :generate_ident_str
  scope :filter, -> (filter) { where('UPPER(name) LIKE UPPER(?)', "%#{filter}%") }

  private

  def generate_ident_str
    if self.ident_str.blank?
      ident_str = SecureRandom.hex(10)
      while self.class.where(ident_str: ident_str).any?
        ident_str = SecureRandom.hex(10)
      end
      self.ident_str = ident_str
    end
  end
end
