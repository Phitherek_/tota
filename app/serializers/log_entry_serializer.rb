class LogEntrySerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :callsign_from, :toilet_id_from, :callsign_to, :toilet_id_to, :band, :mode, :qso_at, :confirmed
end
