class UserSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :login, :callsign, :admin
end
