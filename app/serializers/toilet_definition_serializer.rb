class ToiletDefinitionSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :identifier, :points
end
