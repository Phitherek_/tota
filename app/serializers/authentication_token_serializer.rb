class AuthenticationTokenSerializer
  include FastJsonapi::ObjectSerializer
  attributes :token
end
