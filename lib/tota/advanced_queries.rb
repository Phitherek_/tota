module Tota
  module AdvancedQueries
    def self.activations_query(instance, user = nil)
    <<SQL
SELECT DISTINCT ON(log_entries.user_id, log_entries.callsign_to, log_entries.toilet_id_to, date_trunc('day', log_entries.qso_at)) log_entries.user_id, log_entries.callsign_to, log_entries.toilet_id_to, date_trunc('day', log_entries.qso_at) AS qso_at, toilet_definitions.points, COUNT(ac.*) >= #{instance.activation_needed_qsos} AS confirmed FROM "log_entries" LEFT OUTER JOIN "toilet_definitions" ON log_entries.toilet_id_to = toilet_definitions.identifier LEFT OUTER JOIN (SELECT callsign_from, toilet_id_from, qso_at, confirmed FROM log_entries AS le) ac ON ac.toilet_id_from = log_entries.toilet_id_to AND ac.callsign_from = log_entries.callsign_to AND ac.qso_at >= date_trunc('day', log_entries.qso_at) AND ac.qso_at <= date_trunc('day', log_entries.qso_at) + interval '1 day' - interval '1 second'#{instance.activation_require_confirmation ? ' AND ac.confirmed = TRUE' : ''} WHERE log_entries.toilet_id_to <> '' AND "log_entries"."instance_id" = #{instance.id}#{user.present? ? " AND log_entries.user_id = #{user.id}" : ''} GROUP BY log_entries.user_id, log_entries.callsign_to, log_entries.toilet_id_to, log_entries.qso_at, toilet_definitions.points ORDER BY date_trunc('day', log_entries.qso_at) DESC
SQL
    end

    def self.ranking_query(instance, user = nil, limit = nil)
      <<SQL
SELECT * FROM (SELECT ranking.user_id, ranking.callsign, ranking.activator_points, ranking.chaser_points, ranking.activator_points + ranking.chaser_points AS total_points FROM (SELECT users.id AS user_id, users.callsign, COALESCE(SUM(activations.points), 0) AS activator_points, COALESCE(COUNT(chases.*), 0) AS chaser_points FROM users LEFT OUTER JOIN (#{activations_query(instance, user)}) activations ON activations.user_id = users.id AND (activations.confirmed = TRUE OR activations.confirmed IS NULL) LEFT OUTER JOIN (SELECT user_id, callsign_from FROM log_entries WHERE log_entries.toilet_id_from IS NOT NULL AND log_entries.instance_id = #{instance.id}) chases ON chases.user_id = users.id GROUP BY users.callsign, users.id) ranking#{user.present? ? " WHERE ranking.user_id=#{user.id}" : ''}) inner_ranking ORDER BY inner_ranking.total_points DESC, inner_ranking.activator_points DESC, inner_ranking.chaser_points DESC, inner_ranking.callsign ASC#{limit.present? ? " LIMIT #{limit}" : ''}
SQL
    end
  end
end