namespace :tota do
  desc 'Check entries'
  task :check_entries => :environment do
    LogEntry.find_each do |le|
      entry = le.class.where(callsign_from: le.callsign_to, callsign_to: le.callsign_from).where('toilet_id_from = ? OR toilet_id_to = ?', le.toilet_id_to, le.toilet_id_from).where('qso_at >= ? AND qso_at <= ?', le.qso_at - 5.minutes, le.qso_at + 5.minutes).first
      if entry.present?
        le.update_column(:confirmed, true)
        entry.update_column(:confirmed, true)
      else
        le.update_column(:confirmed, false)
      end
    end
  end
  desc 'Drop callsign breaks'
  task :drop_callsign_breaks => :environment do
    User.find_each do |u|
      u.callsign = u.callsign.split('/')[0].upcase
      u.save(validate: false)
    end
    LogEntry.find_each do |le|
      le.callsign_from = le.callsign_from.try(:split, '/').try(:[], 0).try(:upcase)
      le.callsign_to = le.callsign_to.try(:split, '/').try(:[], 0).try(:upcase)
      le.save(validate: false)
    end
  end
end
