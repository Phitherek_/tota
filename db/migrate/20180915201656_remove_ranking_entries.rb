class RemoveRankingEntries < ActiveRecord::Migration[5.2]
  def change
    drop_table :ranking_entries
  end
end
