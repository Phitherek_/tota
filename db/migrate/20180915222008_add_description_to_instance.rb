class AddDescriptionToInstance < ActiveRecord::Migration[5.2]
  def change
    add_column :instances, :description, :text
  end
end
