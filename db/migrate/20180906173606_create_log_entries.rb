class CreateLogEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :log_entries do |t|
      t.belongs_to :user
      t.string :callsign_from
      t.string :callsign_to
      t.string :toilet_id_from
      t.string :toilet_id_to
      t.string :band
      t.string :mode
      t.timestamp :qso_at
      t.boolean :confirmed
      t.timestamps
    end
  end
end
