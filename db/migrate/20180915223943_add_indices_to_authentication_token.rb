class AddIndicesToAuthenticationToken < ActiveRecord::Migration[5.2]
  def change
    add_index :authentication_tokens, :token, unique: true
  end
end
