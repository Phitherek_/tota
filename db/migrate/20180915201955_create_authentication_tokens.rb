class CreateAuthenticationTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :authentication_tokens do |t|
      t.belongs_to :instance
      t.belongs_to :user
      t.string :token, null: false, default: ''
      t.timestamps
    end
  end
end
