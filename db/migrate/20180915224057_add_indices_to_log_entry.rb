class AddIndicesToLogEntry < ActiveRecord::Migration[5.2]
  def change
    add_index :log_entries, :callsign_from
    add_index :log_entries, :callsign_to
  end
end
