class CreateInstances < ActiveRecord::Migration[5.2]
  def change
    create_table :instances do |t|
      t.string :ident_str, null: false, default: ''
      t.string :name, null: false, default: ''
      t.integer :activation_needed_qsos, null: false, default: 2
      t.boolean :activation_require_confirmation, null: false, default: false
      t.timestamps
    end
  end
end
