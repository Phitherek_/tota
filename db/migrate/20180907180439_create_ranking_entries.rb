class CreateRankingEntries < ActiveRecord::Migration[5.2]
  def change
    create_table :ranking_entries do |t|
      t.belongs_to :user
      t.integer :activator_points
      t.integer :chaser_points
      t.timestamps
    end
  end
end
