class CreateToiletDefinitions < ActiveRecord::Migration[5.2]
  def change
    create_table :toilet_definitions do |t|
      t.belongs_to :instance
      t.string :identifier, null: false, default: ''
      t.integer :points, null: false, default: 1
      t.timestamps
    end
  end
end
