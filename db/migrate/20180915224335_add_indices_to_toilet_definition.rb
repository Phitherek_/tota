class AddIndicesToToiletDefinition < ActiveRecord::Migration[5.2]
  def change
    add_index :toilet_definitions, :identifier
    add_index :toilet_definitions, [:instance_id, :identifier], unique: true
  end
end
