class AddInstanceIdToLogEntry < ActiveRecord::Migration[5.2]
  def change
    add_column :log_entries, :instance_id, :integer
  end
end
