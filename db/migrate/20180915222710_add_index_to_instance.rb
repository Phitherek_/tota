class AddIndexToInstance < ActiveRecord::Migration[5.2]
  def change
    add_index :instances, :ident_str, unique: true
  end
end
