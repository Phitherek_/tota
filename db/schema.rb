# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_15_224335) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "authentication_tokens", force: :cascade do |t|
    t.bigint "instance_id"
    t.bigint "user_id"
    t.string "token", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["instance_id"], name: "index_authentication_tokens_on_instance_id"
    t.index ["token"], name: "index_authentication_tokens_on_token", unique: true
    t.index ["user_id"], name: "index_authentication_tokens_on_user_id"
  end

  create_table "instances", force: :cascade do |t|
    t.string "ident_str", default: "", null: false
    t.string "name", default: "", null: false
    t.integer "activation_needed_qsos", default: 2, null: false
    t.boolean "activation_require_confirmation", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.index ["ident_str"], name: "index_instances_on_ident_str", unique: true
  end

  create_table "log_entries", force: :cascade do |t|
    t.bigint "user_id"
    t.string "callsign_from"
    t.string "callsign_to"
    t.string "toilet_id_from"
    t.string "toilet_id_to"
    t.string "band"
    t.string "mode"
    t.datetime "qso_at"
    t.boolean "confirmed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "instance_id"
    t.index ["callsign_from"], name: "index_log_entries_on_callsign_from"
    t.index ["callsign_to"], name: "index_log_entries_on_callsign_to"
    t.index ["user_id"], name: "index_log_entries_on_user_id"
  end

  create_table "toilet_definitions", force: :cascade do |t|
    t.bigint "instance_id"
    t.string "identifier", default: "", null: false
    t.integer "points", default: 1, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["identifier"], name: "index_toilet_definitions_on_identifier"
    t.index ["instance_id", "identifier"], name: "index_toilet_definitions_on_instance_id_and_identifier", unique: true
    t.index ["instance_id"], name: "index_toilet_definitions_on_instance_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "login", default: "", null: false
    t.string "callsign", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "admin", default: false, null: false
  end

end
