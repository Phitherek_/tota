# Dokumentacja API

## Wprowadzenie

To jest dokumentacja API systemu logowania dla programu TOTA. Ze względu na powtarzalność pewnych parametrów i odpowiedzi wprowadzono następujące tagi, którymi są oznaczane przykłady:
* _instancja_ - zawsze przyjmuje parametr `instance_key` , który jest kluczem instancji (administrator może go odczytać w panelu administracyjnym po stworzeniu instancji). Jeżeli instancja nie zostanie znaleziona, zwracana jest odpowiedź `403 Forbidden` z następującą zawartością JSON:

```json
{ "error": "invalid_instance" }
```

* _token_ - wymaga nagłówka `Authorization: Token <token>`, gdzie `<token>` to token zwrócony z akcji logowania użytkownika. Odpowiedź `403 Forbidden` jest zwracana w następujących przypadkach:
    * brak nagłówka - z zawartością JSON:
    ```json
    { "error": "missing_auth_header" }
    ```
    * zły format nagłówka, brak instancji i/lub użytkownika definiowanych przez podany token - z zawartością JSON:
    ```json
    { "error": "invalid_auth" }
    ```

Dodatkowo zdefiniowano następujące pojęcia:

* _Błędy walidacji_ - są to błędy wynikające z niespełniania przez dany obiekt wytycznych wymaganych przez nawigacje. Gdziekolwiek pojawiają się w odpowiedzi JSON, mają następujący format (gdzie  klucz`atrybut_obiektu` może dotyczyć konkretnego atrybutu lub mieć wartość `base` - wówczas wiadomości błędów zawarte w tablicy będącej wartością tego klucza odnoszą się do całej instancji obiektu):

```json
{ "atrybut_obiektu": [ "wiadomość błędu 1 dla atrybutu atrybut_obiektu", "wiadomość błędu 2 dla atrybutu atrybut_obiektu" ], "kolejny_atrybut_obiektu": [ "kolejne wiadomości błędów dla atrybutu kolejny_atrybut_obiektu" ] }
```

Obowiązują dodatkowo następujące reguły:

* Jeżeli argumenty nie mają adnotacji, trzeba je uznawać za wymagane

## Rekomendacje

* Ze względu na to, że w zapytaniach przesyłane są dane wrażliwe, zaleca się używania połączeń HTTPS

## Akcje dotyczące użytkowników

* `POST /api/users/register`
    * Opis: Rejestracja nowego użytkownika
    * Argumenty: `login` - login, `callsign` - znak wywoławczy,`password` - hasło, `password_confirmation` - potwierdzenie hasła
    * Możliwe odpowiedzi:
        * Sukces - `200 OK` z zawartością JSON:
        ```json
        { "success": "success" }
        ```
        * Błąd walidacji - `422 Unprocessable Entity` z zawartością JSON:
        ```json
        { "error": "user_validation", "details": <Błędy walidacji> }
        ```
* `POST /api/users/login`
    * Opis: Logowanie użytkownika - pozyskanie tokenu uwierzytelniania
    * Argumenty: `instance_key` - klucz instancji, `login` - login, `password` - hasło
    * Możliwe odpowiedzi:
        * Sukces - `200 OK` z zawartością JSON:
        ```json
        { "token": "<token uwierzytelniania>" }
        ```
        * Brak instancji - `404 Not Found` z zawartością JSON:
        ```json
        { "error": "no_instance" }
        ```
        * Zły login lub hasło - `403 Unauthorized` z zawartością JSON:
        ```json
        { "error": "invalid_auth" }
        ```
        * Błąd walidacji - `422 Unprocessable Entity` z zawartością JSON:
        ```json
        { "error": "token_validation", "details": <Błędy walidacji> }
        ```
* `DELETE /api/users/logout` _token_
    * Opis: Wylogowanie użytkownika - usunięcie tokenu uwierzytelnienia
    * Argumenty: (brak)
    * Możliwe odpowiedzi:
        * Sukces - `200 OK` z zawartością JSON:
        ```json
        { "success": "success" }
        ```
        * Błąd walidacji - `422 Unprocessable Entity` z zawartością JSON:
        ```json
        { "error": "token_validation", "details": <Błędy walidacji> }
        ```
* `PUT/PATCH /api/users/profile` _token_
    * Opis: Aktualizacja profilu obecnie zalogowanego użytkownika
    * Argumenty: `current_password` - obecne hasło, `callsign` - znak wywoławczy, `password` - hasło, `password_confirmation` - potwierdzenie hasła
    * Możliwe odpowiedzi:
        * Sukces - `200 OK` z zawartością JSON:
        ```json
        { "success": "success", "object": { "data": { "id": "<id użytkownika>", "type": "user", "attributes": { "id": <id użytkownika>, "login": <login użytkownika>, "callsign": <znak wywoławczy użytkownika>, "admin": <czy użytkownik jest administratorem> } } } }
        ```
        * Nieprawidłowe hasło - `403 Forbidden` z zawartością JSON:
        ```json
        { "error": "invalid_auth" }
        ```
        * Błąd walidacji - `422 Unprocessable Entity` z zawartością JSON:
        ```json
        { "error": "user_validation", "details": <Błędy walidacji> }
        ```
* `GET /api/users/me` _token_
    * Opis: Profil obecnie zalogowanego użytkownika
    * Argumenty: (brak)
    * Możliwe odpowiedzi:
        * Informacje profilowe użytkownika - `200 OK` z zawartością JSON:
        ```json
        { "data": { "id": "<id użytkownika>", "type": "user", "attributes": { "id": <id użytkownika>, "login": <login użytkownika>, "callsign": <znak wywoławczy użytkownika>, "admin": <czy użytkownik jest administratorem> } } }
        ```
## Akcje dotyczące definicji toalet

* `GET /api/toilet_definitions` _instancja_
    * Opis: Definicje toalet dla danej instancji
    * Argumenty: (brak)
    * Możliwe odpowiedzi:
        * Definicje toalet posortowane rosnąco po identyfikatorze toalety - `200 OK` z zawartością JSON:
        ```json
        { "data": [ { "id": "<id definicji>", "type": "toilet_definition", "attributes": { "id": <id definicji>, "identifier": "<identyfikator toalety>", "points": <punkty przyznawane za toaletę> } }, <kolejne definicje podobnie> ] }
        ```

## Akcje dotyczące wpisów

* `GET /api/log_entries` _token_
    * Opis: Ostatnie wpisy obecnie zalogowanego użytkownika dla obecnej instancji posortowane malejąco po czasie łączności
    * Argumenty: `limit` - _(opcjonalnie)_ limit ilości wyświetlonych wpisów
    * Możliwe odpowiedzi:
        * Lista wpisów - `200 OK` z zawartością JSON:
        ```json
        { "data": [ { "id": "<id wpisu>", "type": "log_entry", "attributes": { "id": <id wpisu>, "callsign_from": "<znak wywoławczy rozmówcy>", "toilet_id_from": "<identyfikator toalety rozmówcy>", "callsign_to": "<znak wywoławczy logującego>", "toilet_id_to": "<identyfikator toalety logującego>", "band": "<pasmo>", "mode": "<emisja>", "qso_at": "<data i godzina łączności UTC>", "confirmed": "<czy wpis jest potwierdzony>" } }, <kolejne wpisy podobnie> ]}
        ```
* `POST /api/log_entries` _token_
    * Opis: Nowy wpis dla obecnie zalogowanego użytkownika i obecnej instancji
    * Argumenty: `callsign_from` - znak wywoławczy rozmówcy, `toilet_id_from` - identyfikator toalety rozmówcy _(może być pusty jeżeli `toilet_id_to` jest wypełniony)_, `callsign_to` - znak wywoławczy logującego, `toilet_id_to` - identyfikator toalety logującego _(może być pusty jeżeli `toilet_id_from` jest wypełniony)_, `band` - pasmo _(możliwe wartości: 160m, 80m, 60m, 40m, 30m, 20m, 17m, 15m, 12m, 10m, 6m, 2m, 70cm, 23cm)_, `mode` - emisja _(możliwe wartości: CW, SSB, FM, Dane, AM, Inna emisja)_, `qso_at` - data i czas łączności UTC _(rekomendowany format: DD-MM-YYYY HH:MM)_
    * Możliwe odpowiedzi:
        * Sukces - `200 OK` z zawartością JSON:
        ```json
        { "success": "success", "object": { "data": { "id": "<id wpisu>", "type": "log_entry", "attributes": { "id": <id wpisu>, "callsign_from": "<znak wywoławczy rozmówcy>", "toilet_id_from": "<identyfikator toalety rozmówcy>", "callsign_to": "<znak wywoławczy logującego>", "toilet_id_to": "<identyfikator toalety logującego>", "band": "<pasmo>", "mode": "<emisja>", "qso_at": "<data i godzina łączności UTC>", "confirmed": "<czy wpis jest potwierdzony>" } } } }
        ```
        * Błąd walidacji - `422 Unprocessable Entity` z zawartością JSON:
        ```json
        { "error": "log_entry_validation", "details": <Błędy walidacji> }
        ```
* `GET /api/log_entries/:id` _token_
    * Opis: Konkretny wpis obecnie zalogowanego użytkownika w obecnej instancji
    * Argumenty ścieżki: `:id` - identyfikator wpisu
    * Argumenty: (brak)
    * Możliwe odpowiedzi:
        * Dane wpisu - `200 OK` z zawartością JSON:
        ```json
        { "data": { "id": "<id wpisu>", "type": "log_entry", "attributes": { "id": <id wpisu>, "callsign_from": "<znak wywoławczy rozmówcy>", "toilet_id_from": "<identyfikator toalety rozmówcy>", "callsign_to": "<znak wywoławczy logującego>", "toilet_id_to": "<identyfikator toalety logującego>", "band": "<pasmo>", "mode": "<emisja>", "qso_at": "<data i godzina łączności UTC>", "confirmed": "<czy wpis jest potwierdzony>" } } }
        ```
        * Brak wpisu - `404 Not Found` z zawartością JSON:
        ```json
        { "error": "log_entry_not_found" }
        ```
* `PUT/PATCH /api/log_entries/:id` _token_
    * Opis: Aktualizacja konkretnego wpisu obecnie zalogowanego użytkownika w obecnej instancji
     * Argumenty ścieżki: `:id` - identyfikator wpisu
     * Argumenty: `callsign_from` - znak wywoławczy rozmówcy, `toilet_id_from` - identyfikator toalety rozmówcy _(może być pusty jeżeli `toilet_id_to` jest wypełniony)_, `callsign_to` - znak wywoławczy logującego, `toilet_id_to` - identyfikator toalety logującego _(może być pusty jeżeli `toilet_id_from` jest wypełniony)_, `band` - pasmo _(możliwe wartości: 160m, 80m, 60m, 40m, 30m, 20m, 17m, 15m, 12m, 10m, 6m, 2m, 70cm, 23cm)_, `mode` - emisja _(możliwe wartości: CW, SSB, FM, Dane, AM, Inna emisja)_, `qso_at` - data i czas łączności UTC _(rekomendowany format: DD-MM-YYYY HH:MM)_
     * Możliwe odpowiedzi:
         * Sukces - `200 OK` z zawartością JSON:
         ```json
         { "success": "success", "object": { "data": { "id": "<id wpisu>", "type": "log_entry", "attributes": { "id": <id wpisu>, "callsign_from": "<znak wywoławczy rozmówcy>", "toilet_id_from": "<identyfikator toalety rozmówcy>", "callsign_to": "<znak wywoławczy logującego>", "toilet_id_to": "<identyfikator toalety logującego>", "band": "<pasmo>", "mode": "<emisja>", "qso_at": "<data i godzina łączności UTC>", "confirmed": "<czy wpis jest potwierdzony>" } } } }
         ```
         * Brak wpisu - `404 Not Found` z zawartością JSON:
         ```json
         { "error": "log_entry_not_found" }
         ```
         * Błąd walidacji - `422 Unprocessable Entity` z zawartością JSON:
         ```json
         { "error": "log_entry_validation", "details": <Błędy walidacji> }
     
* `DELETE /api/log_entries/:id` _token_
    * Opis: Usunięcie konkretnego wpisu obecnie zalogowanego użytkownika w obecnej instancji
    * Argumenty ścieżki: `:id` - identyfikator wpisu
    * Argumenty: (brak)
    * Możliwe odpowiedzi:
        * Sukces - `200 OK` z zawartością JSON:
        ```json
        { "success": "success" }
        ```
        * Brak wpisu - `404 Not Found` z zawartością JSON:
        ```json
        { "error": "log_entry_not_found" }
        ```
        * Błąd walidacji - `422 Unprocessable Entity` z zawartością JSON:
        ```json
        { "error": "log_entry_validation", "details": <Błędy walidacji> }
* `GET /api/log_entries/top` _instancja_
    * Opis: Ostatnie wpisy dla danej instancji posortowane malejąco po czasie łączności
    * Argumenty: `limit` -  _(opcjonalnie)_ limit ilości wyświetlonych wpisów
    * Możliwe odpowiedzi:
        * Lista wpisów - `200 OK` z zawartością JSON:
        ```json
        { "data": [ { "id": "<id wpisu>", "type": "log_entry", "attributes": { "id": <id wpisu>, "callsign_from": "<znak wywoławczy rozmówcy>", "toilet_id_from": "<identyfikator toalety rozmówcy>", "callsign_to": "<znak wywoławczy logującego>", "toilet_id_to": "<identyfikator toalety logującego>", "band": "<pasmo>", "mode": "<emisja>", "qso_at": "<data i godzina łączności UTC>", "confirmed": "<czy wpis jest potwierdzony>" } }, <kolejne wpisy podobnie> ] }
        ```
* `GET /api/log_entries/activations` _token_
    * Opis: Ostatnie aktywacje użytkownika posortowane malejąco po dniu aktywacji
    * Argumenty: (brak)
    * Możliwe odpowiedzi:
        * Lista aktywacji - `200 OK` z zawartością JSON:
        ```json
        { "activations": [ { "user_id": <id użytkownika>, "callsign_to": "<znak wywoławczy aktywatora>", "toilet_id_to": "<identyfikator aktywowanej toalety>", "qso_at": "<data aktywacji>", "points": "<liczba punktów za potwierdzoną aktywację>", "confirmed": "<czy aktywacja jest potwierdzona zgodnie z wymaganiami instancji>" }, <kolejne aktywacje podobnie> ] }
        ```

## Akcje dotyczące rankingu

* `GET /api/ranking/top` _instancja_
    * Opis: Ranking punktowy użytkowników dla danej instancji, o miejscu w rankingu decydują kolejno suma punktów, punkty za aktywacje, punkty za łowy i ostatecznie rosnąco znak wywoławczy
    * Argumenty: `limit` - _(opcjonalny, wartość domyślna: 10)_ limit ilości wyświetlonych miejsc w rankingu
    * Możliwe odpowiedzi:
        * Ranking - `200 OK` z zawartością JSON:
        ```json
        { "ranking_entries": [ { "user_id": <id użytkownika>, "callsign": "<znak wywoławczy użytkownika>", "activator_points": <liczba punktów aktywatora>, "chaser_points": <liczba punktów łowcy>, "total_points": <suma punktów> }, <dalsze wpisy/miejsca w rankingu podobnie> ] }
        ```
* `GET /api/ranking/my` _token_
    * Opis: Wpis w rankingu dla obecnie zalogowanego użytkownika i obecnej instancji
    * Argumenty: (brak)
    * Możliwe odpowiedzi:
        * Wpis w rankingu - `200 OK` z zawartością JSON:
        ```json
        { "ranking_entry": { "user_id": <id użytkownika>, "callsign": "<znak wywoławczy użytkownika>", "activator_points": <liczba punktów aktywatora>, "chaser_points": <liczba punktów łowcy>, "total_points": <suma punktów> } }
        ```