Rails.application.routes.draw do
  devise_for :users
  resources :users, path: :admin_users, only: [:index, :create, :update, :destroy]
  resources :log_entries, only: [:index, :create, :update, :destroy]
  resources :authentication_tokens, only: [:index, :destroy]
  resources :instances, only: [:index, :create, :update, :destroy]
  resources :toilet_definitions, only: [:index, :create, :update, :destroy]
  namespace :api do
    resources :log_entries, only: [:index, :create, :show, :update, :destroy] do
      collection do
        get :top
        get :activations
      end
    end
    resources :ranking, only: :none do
      collection do
        get :top
        get :my
      end
    end
    resources :toilet_definitions, only: [:index]
    resources :users, only: :none do
      collection do
        get :me
        put :profile
        patch :profile
        post :register
        post :login
        delete :logout
      end
    end
  end
  root to: 'main#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
